package week7;

import lombok.Data;

/**
 * @Description: 请求测试的实体类
 * @Author: easonlee
 * @CreateDate 2020/12/6
 */
@Data
public class RequestTest {

    public RequestTest(String method,String url,String payload){
        this.payload = payload;
        this.method = method;
        this.url = url;
    }

    public Long getCostTm(){
        return finishTm - startTm;
    }

    /**
     * 请求方法，目前支持GET和POST
     */
    private String method;

    private String url;

    private String payload;

    private Long startTm;

    private Long finishTm;

    private Long costTm;
}

package week7;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.List;

/**
 * @Description: TODO
 * @Author: easonlee
 * @CreateDate 2020/12/6
 */
@Slf4j
public class HttpRequestWorker implements Runnable{

    private List<Long> costTimes;
    private int executeSize;
    private String url;
    private boolean finished = false;

    public HttpRequestWorker(String url,List<Long> costTimes,int executeSize){
        this.url = url;
        this.costTimes = costTimes;
        this.executeSize = executeSize;
    }

    public boolean isFinished(){
        return finished;
    }

    @Override
    public void run() {
        RequestTest requestTest = new RequestTest(RequestConstant.GET,url,null);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet request = new HttpGet(requestTest.getUrl());
        CloseableHttpResponse response = null;
        for(int i=0;i<executeSize;i++){
//            log.info("{} run times:{}",Thread.currentThread().getName(),i+1);
//            System.out.println("thread:"+Thread.currentThread()+"running ....size:"+(i+1));
            requestTest.setStartTm(System.currentTimeMillis());
            try {
                response = httpclient.execute(request);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    requestTest.setFinishTm(System.currentTimeMillis());
                    costTimes.add(requestTest.getCostTm());

//                    读取完数据，完成整体的响应
//                    String result = EntityUtils.toString(response.getEntity(),"UTF-8");
//                    System.out.println("result:"+result);

                }else{
                    System.out.println("fail,status:"+response.getStatusLine().getStatusCode());
                }
            } catch (IOException e) {
                log.error("发送请求失败",e);
            }finally {
                if (response != null) {
                    try {
                        response.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        try {
            httpclient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finished = true;
    }
}

package week7;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static week7.RequestConstant.GET;

/**
 * @Description: TODO
 * @Author: easonlee
 * @CreateDate 2020/12/6
 */
@Slf4j
public class BatchHttpTester {

    private String url;
    private int total;
    private int batchSize;
    private List<Long> costTimes;
    private List<HttpRequestWorker> workers;

    public BatchHttpTester(String url, int total, int batchSize){
        this.url = url;
        this.total = total;
        this.batchSize = batchSize;
        this.costTimes = new ArrayList<>(total);
        this.workers = new ArrayList<>(batchSize);
    }

    public void runTest(){
        System.out.println("====== begin do batch test ======");
        System.out.println(String.format("url:%s",url));
        System.out.println(String.format("total:%s",total));
        System.out.println(String.format("batchSize:%s",batchSize));
        System.out.println("start at:"+ LocalDateTime.now());

        int executeSize = total / batchSize;

        for(int i=0;i<batchSize;i++){
            HttpRequestWorker worker = new HttpRequestWorker(url,costTimes,executeSize);
            workers.add(worker);
            new Thread(worker).start();
        }
        while (true){
            int finishSize = workers.stream().mapToInt(w->w.isFinished()?1:0).sum();
            if(finishSize == batchSize){
                break;
            }else{
                System.out.println("checking finish ..... counts:"+costTimes.size());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("finish at:"+ LocalDateTime.now());
        System.out.println("success total:"+ costTimes.size());
        System.out.println("====== finish do batch test ======");
    }

    /***
     * 获取平均响应时间，单位是毫秒
     * @return
     */
    public Double getAverageCostTm(){
        return costTimes.stream()
                .mapToInt(i ->i.intValue()).average().orElse(0D);
    }

    /***
     * 获取请求比例的响应时间，单位是毫秒
     * @param proportion
     * @return
     */
    public Double getProportionPCostTm(int proportion){
        if(proportion <=0 || proportion > 100){
            throw new IllegalStateException("非法的比例范围，请使用1~100之间的值");
        }
        int skipSize = total * (100 - proportion) / 100;
        //倒序，然后跳过部分值，最后将剩余的值取平均值
        return costTimes.stream()
                .sorted(Comparator.comparing(Long::intValue).reversed())
                .skip(skipSize)
                .mapToInt(i ->i.intValue()).average().orElse(0D);
    }

//    public static void main(String[] args) {
//        System.out.println(123*10/100);
//    }
}

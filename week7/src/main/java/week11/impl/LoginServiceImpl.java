package week11.impl;

import org.apache.commons.codec.binary.StringUtils;
import week11.EncryptUtil;
import week11.LoginService;

/**
 * @Description: TODO
 * @Author: easonlee
 * @CreateDate 2021/1/2
 */
public class LoginServiceImpl implements LoginService {

    /***
     * 公共的随机加密盐
     */
    private static final String COMMON_SALT = "5Ilz81VJvjb1Lg8u";

    @Override
    public boolean isClientExist(String clientId) {
        // TODO 这里应该是从数据库里面获取对应的用户信息，演示代码不做实现
        return "rslee".equals(clientId);
    }

    @Override
    public boolean checkPW(String clientId, String password, String secretPassword) {
        if(isClientExist(clientId) && password != null){
            // 基于salt+密码，后算sha256的值进行比对
            String encodePwd = EncryptUtil.SHA256(COMMON_SALT+password);
//            System.out.println("encodePwd:"+encodePwd);
            return encodePwd.equals(secretPassword);
        }
        return false;
    }
}

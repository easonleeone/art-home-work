package week11;

/**
 * @Description: 基于散列加密+盐，进行登录校验
 * @Author: easonlee
 * @CreateDate 2021/1/2
 */
public interface LoginService {

    /***
     * 判断用户是否存在
     * @param clientId
     * @return
     */
    boolean isClientExist(String clientId);

    /***
     * 校验输入的用户名密码是否正确
     * @param clientId 用户对于的唯一ID
     * @param password 用户登录输入的明文密码
     * @param secretPassword  从数据库中取得的加密后的密码
     * @return
     */
    boolean checkPW(String clientId,String password,String secretPassword);
}

package week7;

import org.junit.Test;

/**
 * @Description: TODO
 * @Author: easonlee
 * @CreateDate 2020/12/6
 */
public class MainTest {

    @Test
    public void TestBatchHttpGetTest(){
        String url = "https://www.baidu.com";
        int total = 100;
        int batchSize = 10;
        BatchHttpTester tester = new BatchHttpTester(url,total,batchSize);
        tester.runTest();
        System.out.println("total average cost tm: "+ tester.getAverageCostTm());
        System.out.println("95% average cost tm: "+ tester.getProportionPCostTm(95));
    }
}

package week11;

import org.junit.Test;
import week11.impl.LoginServiceImpl;

import static org.junit.Assert.assertTrue;

/**
 * @Description: 登录服务单元测试
 * @Author: easonlee
 * @CreateDate 2021/1/2
 */
public class LoginServiceTest {

    @Test
    public void testCheckPW(){
        String clientId = "rslee";
        String password = "test123456";
        String secretPassword = "83309a61a62712878c1b4ca0862cd055cc7f8d121cb4672e3f529307325a4b16";
        assertTrue("登录用户密码测试",new LoginServiceImpl().checkPW(clientId,password,secretPassword));
    }
}
